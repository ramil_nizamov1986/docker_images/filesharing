FROM python:3
ENV PYTHONUNBUFFERED 2
RUN mkdir /app
WORKDIR /app
ADD /app /app
RUN pip install -r requirements.txt
CMD python3 manage.py runserver 0.0.0.0:8000
